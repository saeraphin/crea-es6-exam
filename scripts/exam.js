import './helpers/delegateEventListener.js'
import { CardElement, FigureElement, ModalElement } from './elements/elements.js'

window.addEventListener('DOMContentLoaded', function () {
  // Create modal container and append it to the body
  const modal = ModalElement.from({id: 'main-modal'})
  document.body.appendChild(modal)

  // figure example as modal content
  const figureExample = FigureElement.from({
    id: 'figure-example',
    title: 'Fantastic Four',
    imageSrc: 'http://x.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73.jpg'
  })
  // Access modal content
  modal.contentElement.appendChild(figureExample)


  // Search results should be appended to the #search-results list
  const results = document.getElementById('search-results')
  // Don't forget to add <li> elements as direct children of the list
  const baseListItem = document.createElement('li')


  // Dummy content, delete me when you have results
  for (let i = 0; i < 10; i++) {
    const item = baseListItem.cloneNode()
    // Card creation example, use static .from() method
    const card = CardElement.from({
      id: 'characterId',
      title: 'Character name',
      subtitle: 'Last modified',
      // cardImageSrc: 'someUrl',
      // avatarImageSrc: 'someUrl',
    })
    // Access card content
    // card.contentElement.innerHTML
    item.appendChild(card)
    results.appendChild(item)
  }
})
