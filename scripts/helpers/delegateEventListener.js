
/**
 * Side Effect only
 * Create delegateEventListener method
 * 
 * Inspired by:
 * https://stackoverflow.com/questions/23508221/vanilla-javascript-event-delegation/23978597
 */

(function(EventTarget) {
  if (typeof EventTarget.prototype.delegateEventListener !== 'undefined') {
    console.warn('EventTarget.prototype.delegateEventListener already exists', 'aborting side effect')
    return false
  }

  /**
   * Add a delegate event listener to the element
   * Extend the EventTarget prototype to add a delegateEventListener() event
   * @param {string} eventType the type of event to listen for
   * @param {string} selector a CSS selector of the items that will trigger the callback
   * @param {function} callback the callback function to be triggered
   */
  EventTarget.prototype.delegateEventListener = function(eventType, selector, callback) {
    this.addEventListener(eventType, function(event) {
      const delegateTarget = event.target.closest(selector)

      if (delegateTarget) {
        callback.call(delegateTarget, event)
      }
    })
  }

}(window.EventTarget || window.Element))
