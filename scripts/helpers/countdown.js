
const countdownTimeout = 1000

const secondMs = 1000
const minuteMs = secondMs * 60
const hourMs = minuteMs * 60
const dayMs = hourMs * 24

const getDistanceToDate = (endDate) => {
  const now = new Date()
  const distance = endDate.getTime() - now.getTime()
  const absDistance = Math.abs(distance)

  return {
    d: Math.floor(absDistance / dayMs),
    h: Math.floor((absDistance % dayMs) / hourMs),
    m: Math.floor((absDistance % hourMs) / minuteMs),
    s: Math.floor((absDistance % minuteMs) / secondMs),
    expired: distance < 0
  }
}

const padAll = (...txts) => txts.map(txt => `${txt}`.padStart(2, '0'))

const updateElement = (element, endDate) => {
  const {d, h, m, s, expired} = getDistanceToDate(endDate)
  const [pd, ph, pm, ps] = padAll(d, h, m, s)
  const timeString = `${pd}d ${ph}h ${pm}m ${ps}s`

  element.textContent = timeString

  if (!expired) {
    element.classList.add('is-success')
    element.classList.remove('is-danger')
  } else {
    element.classList.remove('is-success')
    element.classList.add('is-danger')
  }

  setTimeout(updateElement, countdownTimeout, element, endDate)
}

export const setCountdownOnElement = (element, endDate) => {
  if (!(element instanceof HTMLElement)) {
    throw new Error('Target element should be an instance of HTMLElement!')
  }
  if (!(endDate instanceof Date)) {
    throw new Error('End date should be an instance of Date!')
  }

  updateElement(element, endDate)
}
