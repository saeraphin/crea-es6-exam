import './helpers/setupNav.js'
import {setCountdownOnElement} from './helpers/countdown.js'

window.addEventListener('DOMContentLoaded', function () {

  const countdown = document.getElementById('countdown')
  const endDate = new Date(2023, 2, 26, 23, 59, 59, 999)
  setCountdownOnElement(countdown, endDate)

})
