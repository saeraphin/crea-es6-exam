const template = /*html*/ `
  <div class="card-image">
    <figure class="image is-4by3">
      <img src="https://bulma.io/images/placeholders/1280x960.png">
    </figure>
  </div>
  <div class="card-content">
    <div class="media">
      <div class="media-left">
        <figure class="image is-48x48">
          <img src="https://bulma.io/images/placeholders/96x96.png">
        </figure>
      </div>
      <div class="media-content">
        <p class="title is-4"></p>
        <p class="subtitle is-6"></p>
      </div>
    </div>

    <div class="content"></div>
  </div>
`

export class CardElement extends HTMLElement {
  #cardImage
  #avatarImage
  #contentElement
  #titleElement
  #subtitleElement

  constructor({id, title = '', subtitle = '', cardImageSrc, avatarImageSrc}) {
    super()
    this.id = id
    this.classList.add('card')
    this.innerHTML = template

    this.#cardImage = this.querySelector('.card-image img')
    this.#avatarImage = this.querySelector('.media-left img')
    this.#contentElement = this.querySelector('.content')
    this.#titleElement = this.querySelector('.media-content .title')
    this.#subtitleElement = this.querySelector('.media-content .subtitle')

    this.title = title
    this.subtitle = subtitle
    this.cardImageSrc = cardImageSrc || this.cardImageSrc
    this.avatarImageSrc = avatarImageSrc || this.avatarImageSrc
  }

  get contentElement() {
    return this.#contentElement
  }

  get title() {
    return this.#titleElement.textContent
  }
  set title(value) {
    this.#titleElement.textContent = value
    this.#cardImage.setAttribute('alt', `${value} illustration`)
    this.#avatarImage.setAttribute('alt', `${value} avatar image`)
  }

  get subtitle() {
    return this.#subtitleElement.textContent
  }
  set subtitle(value) {
    this.#subtitleElement.textContent = value
    this.#subtitleElement.style.display = !!value ? undefined : 'none'
  }

  get cardImageSrc() {
    return this.#cardImage.getAttribute('src')
  }
  set cardImageSrc(value) {
    this.#cardImage.setAttribute('src', value)
  }

  get avatarImageSrc() {
    return this.#avatarImage.getAttribute('src')
  }
  set avatarImageSrc(value) {
    this.#avatarImage.setAttribute('src', value)
  }

  static from = opts => new CardElement(opts)
}

/**
 * Side effect
 * Add ModalElement to the custom elements registry
 */
customElements.define('element-card', CardElement)
