import '../helpers/delegateEventListener.js'

const template = /*html*/ `
  <div class="modal-background"></div>
  <div class="modal-content"></div>
  <button class="modal-close is-large" aria-label="close"></button>
`

export class ModalElement extends HTMLElement {
  #backgroundElement
  #contentElement
  #closeButton

  constructor({id}) {
    super()
    this.setAttribute('id', id)
    this.setAttribute('class', 'modal')
    this.innerHTML = template

    this.#backgroundElement = this.querySelector('.modal-background')
    this.#contentElement = this.querySelector('.modal-content')
    this.#closeButton = this.querySelector('.modal-close')

    this.#backgroundElement.addEventListener('click', this.close)
    this.#closeButton.addEventListener('click', this.close)
  }

  get backgroundElement() {
    return this.#backgroundElement
  }

  get contentElement() {
    return this.#contentElement
  }

  open = () => {
    this.classList.add('is-active')
    document.documentElement.classList.add('is-clipped')
  }

  close = () => {
    this.classList.remove('is-active')
    document.documentElement.classList.remove('is-clipped')
  }

  static from = opts => new ModalElement(opts)
}

/**
 * Side effect
 * Add ModalElement to the custom elements registry
 */
customElements.define('element-modal', ModalElement)

/**
 * Side effect
 * Add delegate event listener to ".modal-open" elements
 */
document.delegateEventListener('click', '.modal-open', function () {
  const currentTarget = this
  const targetModalId = currentTarget?.getAttribute('data-target')
  const targetModal = document.getElementById(targetModalId)
  targetModal?.open()
})
