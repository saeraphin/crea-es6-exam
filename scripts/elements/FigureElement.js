const template = /*html*/ `
  <figure class="image">
    <img src="https://bulma.io/images/placeholders/320x480.png">
    <figcaption class="is-absolute"></figcaption>
  </figure>
`

export class FigureElement extends HTMLElement {
  #image
  #captionElement

  constructor({id, title = '', imageSrc}) {
    super()
    this.id = id
    this.innerHTML = template

    this.#image = this.querySelector('img')
    this.#captionElement = this.querySelector('figcaption')

    this.title = title
    this.imageSrc = imageSrc || this.imageSrc
  }

  get title() {
    return this.#captionElement.textContent
  }
  set title(value) {
    this.#captionElement.textContent = value
    this.#image.setAttribute('alt', `${value} illustration`)
  }

  get imageSrc() {
    return this.#image.getAttribute('src')
  }
  set imageSrc(value) {
    this.#image.setAttribute('src', value)
  }

  static from = opts => new FigureElement(opts)
}

/**
 * Side effect
 * Add ModalElement to the custom elements registry
 */
customElements.define('element-figure', FigureElement)
