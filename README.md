# CREA ES6 course exam

To launch the exam, type:

```Shell
npx http-server . -o
```

This will run a simple HTTP server on the port 8080 and open a browser window once started.

## Remark

If your 8080 port is taken, you can use the ```-p PortNumber``` option to specify the port to be used.

```Shell
npx http-server . -p 8042 -o
```


More info: https://www.npmjs.com/package/http-server